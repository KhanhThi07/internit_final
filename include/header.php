<?php
session_start();
error_reporting(0);

include('connection/db.php');

$header=mysqli_query($conn,"select nd.*,sv.* from nguoi_dung nd, sinh_vien sv
            where nd.ma_nd=sv.ma_taikhoan and email ='{$_SESSION['email']}' ");
while ($row=mysqli_fetch_array($header)) {
  $hinh_anh=$row['hinh_anh'];
  $ho_ten=$row['ho_ten'];
}

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Tuyển dụng thực tập sinh IT</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
   

       

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.php">INTERNIT</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item <?php if($page=='home'){echo 'active';} ?>"><a href="index.php" class="nav-link">Trang chủ</a></li>
	          <li class="nav-item <?php if($page=='about'){echo 'active';} ?>"><a href="http://localhost/final_intern_it/index.php#cong_ty" class="nav-link">Công ty</a></li>
	          <li class="nav-item <?php if($page=='blog'){echo 'active';} ?>"><a href="blog.php" class="nav-link">Tuyển dụng</a></li>
	          <li class="nav-item <?php if($page=='contact'){echo 'active';} ?>"><a href="contact.php" class="nav-link">Liên hệ</a></li>
            <?php
              if (isset($_SESSION['email'])==true) {?>
                <li class="nav-item cta mr-md-2"><a href="job-post.php" class="nav-link">
                    <?php if(empty($ho_ten)){echo $_SESSION['email'];}else{echo $ho_ten;} ?></a></li>
              
                <li class="nav-item">
                  <div class="dropdown">
                    <img src="profile_img/<?php if(empty($hinh_anh)){echo "logo.png";}else{echo $hinh_anh;} ?>" 
                    class="img-circle dropdown-toggle" type="button" data-toggle="dropdown" alt="Cinque Terre" width="50" height="50">
                    <ul class="dropdown-menu" style="background-color: rgba(255, 255, 255, 0.3);">
                      <li><a href="my-profile.php" style="color: #000;">Thông tin cá nhân</a></li>
                      <li><a href="logout.php"  style="color: #000;">Đăng xuất</a></li>
                    </ul>                    
                  </div>
                </li>

            <?php
              }else{?>
	          <li class="nav-item cta mr-md-2"><a href="job-post.php" class="nav-link">Đăng nhập</a></li>
            <?php } ?>

	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->
    
