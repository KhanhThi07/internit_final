<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Nhà tuyển dụng đăng ký</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center" style="background-image:linear-gradient(to bottom,rgba(255, 99, 71, 0) 0%,rgba(255, 99, 71, 0) 100%), url('https://images.pexels.com/photos/1229861/pexels-photo-1229861.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');">
    <form class="form-signin" style="background-color: #fff; border: 3px solid #00bcd4 ;" action="sign_up_nhatuyendung.php" method="POST">
      <img class="mb-4" src="images/logo.png" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Vui lòng đăng ký</h1>

      <label for="email" class="sr-only">Email</label>
      <input type="email" id="email" name="email" class="form-control" placeholder="Địa chỉ email đăng nhập" required autofocus>

      <label for="mat_khau" class="sr-only">Mật khẩu</label>
      <input type="password" id="mat_khau" name="mat_khau" class="form-control" placeholder="Mật khẩu" required>

      <label for="ten_congty" class="sr-only">Tên công ty</label>
      <input type="text" id="ten_congty" class="form-control" name="ten_congty" placeholder="Tên công ty" required autofocus>

      <label for="dia_chi" class="sr-only">Địa chỉ</label>
      <input type="text" id="dia_chi" class="form-control" name="dia_chi" placeholder="Địa chỉ" required autofocus>

      <label for="quy_mo" class="sr-only">Quy mô</label>
      <input type="text" id="quy_mo" class="form-control" name="quy_mo" placeholder="Quy mô" required autofocus>

         <label for="website" class="sr-only">Địa chỉ website</label>
      <input type="text" id="website" class="form-control" name="website" placeholder="Địa chỉ website" required autofocus>

      <label for="ten_nhanvien" class="sr-only">Tên nhân viên</label>
      <input type="text" id="ten_nhanvien" class="form-control" name="ten_nhanvien" placeholder="Tên nhân viên" required autofocus>

      <label for="ngay_sinh" class="sr-only">Ngày sinh</label>
      <input type="date" id="ngay_sinh" class="form-control" name="ngay_sinh" placeholder="Ngày sinh" required autofocus>
   
      <label for="gioi_tinh" class="sr-only">Giới tính</label>
      <input type="text" id="gioi_tinh" class="form-control" name="gioi_tinh" placeholder="Giới tính" required autofocus>

      <label for="so_dien_thoai" class="sr-only">Số điện thoại</label>
      <input type="number" id="so_dien_thoai" class="form-control" name="so_dien_thoai" placeholder="Số điện thoại" required autofocus>

      <label for="chuc_vu" class="sr-only">Chức vụ</label>
      <input type="text" id="chuc_vu" class="form-control" name="chuc_vu" placeholder="Chức vụ" required autofocus>

      <label for="cccd" class="sr-only">Căn cước công dân</label>
      <input type="number" id="cccd" class="form-control" name="cccd" placeholder="Căn cước công dân" required autofocus>

      <!-- <label for="truong_hoc" class="sr-only">Trường học</label>
      <input type="text" id="truong_hoc" class="form-control" name="truong_hoc" placeholder="Trường học" required autofocus> -->
      
     

      <input  type="submit" class="btn btn-lg btn-primary btn-block"style="background-color: #00bcd4; border-color: #00bcd4;" name="submit" value="Đăng ký">
      <a href="job-post.php" style="color:  #00bcd4;">Đã có tài khoản</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2021-2022</p>
    </form>
  </body>
</html>
<?php        
error_reporting(0);
    include('connection/db.php');
   
    if (isset($_POST['submit'])) {

        $email =$_POST['email'];
        $mat_khau =$_POST['mat_khau'];
        $vai_tro ='2';
       

        $ten_congty =$_POST['ten_congty'];
        $dia_chi =$_POST['dia_chi'];
        $quy_mo =$_POST['quy_mo'];
        $website =$_POST['website'];

        $ten_nhanvien =$_POST['ten_nhanvien'];  
        $ngay_sinh =$_POST['ngay_sinh'];
        $gioi_tinh =$_POST['gioi_tinh'];
        $so_dien_thoai =$_POST['so_dien_thoai'];
        $chuc_vu =$_POST['chuc_vu'];
        // $mo_ta =$_POST['mo_ta'];
        $cccd =$_POST['cccd'];
       
       $query = mysqli_query($conn,"insert into nguoi_dung(email,mat_khau,vai_tro)value('$email','$mat_khau','$vai_tro')");
      
       if ($query) {         

          $query1 = mysqli_query($conn,"insert into nha_tuyen_dung(ten_congty,dia_chi,quy_mo,website)
          value('$ten_congty','$dia_chi','$quy_mo',' $website')");

          if($query1){

            $query2=mysqli_query($conn,'select ma_nd from nguoi_dung where ma_nd=(select max(ma_nd) from nguoi_dung )');
            $rowcount=mysqli_num_rows($query2);
            for($i=1;$i<=$rowcount;$i++)
            {
              $row=mysqli_fetch_array($query2);      
              $ma_taikhoan=$row['ma_nd'];;                         
            } 

            $query3=mysqli_query($conn,'select ma_congty from nha_tuyen_dung where ma_congty=(select max(ma_congty) from nha_tuyen_dung )');
            $rowcount=mysqli_num_rows($query3);
            for($i=1;$i<=$rowcount;$i++)
            {
              $row=mysqli_fetch_array($query3);      
              $ma_congty=$row['ma_congty'];;                         
            } 
            

            $sql = mysqli_query($conn,"insert into nhan_vien(ho_ten,ngay_sinh,gioi_tinh,cccd,so_dien_thoai,chuc_vu,
            ma_congty,ma_taikhoan)value(' $ten_nhanvien',' $ngay_sinh','$gioi_tinh',' $cccd',' $so_dien_thoai',
            '$chuc_vu',' $ma_congty','$ma_taikhoan') ");
  
            if($sql){
            echo"<script>alert('Bây giờ bạn có thể đăng nhập!!!');</script>";               
            header('location:job-post.php')      ;
           
          }    
            else{
                echo"<script>alert('Lỗi rồi, vui lòng thử lại!!!');</script>";            
            }
          }
        }
    }  

?>
