-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2022 at 06:25 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intern_it`
--

-- --------------------------------------------------------

--
-- Table structure for table `ho_so_ung_tuyen`
--

CREATE TABLE IF NOT EXISTS `ho_so_ung_tuyen` (
  `ma_hoso` int(10) NOT NULL,
  `ma_tuyendung` int(10) NOT NULL,
  `ma_sinhvien` int(10) NOT NULL,
  `ngay_nop` datetime NOT NULL,
  `file_cv` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ho_so_ung_tuyen`
--

INSERT INTO `ho_so_ung_tuyen` (`ma_hoso`, `ma_tuyendung`, `ma_sinhvien`, `ngay_nop`, `file_cv`) VALUES
(10, 11, 2, '2022-01-11 21:09:19', 'cv_1641542710.pdf'),
(15, 10, 3, '2022-01-11 21:15:40', 'cv_1641542710.pdf'),
(29, 12, 2, '2022-01-12 18:26:27', 'CV_LeKhanhThi_FE.pdf'),
(30, 11, 3, '2022-01-12 18:28:06', 'CV_LeKhanhThi_FE.pdf'),
(33, 14, 2, '2022-01-26 15:05:22', 'cv_1641542710.pdf'),
(37, 13, 6, '2022-02-18 14:14:44', 'CVDEMO.pdf'),
(38, 14, 8, '2022-02-18 14:22:26', 'CVDEMO.pdf'),
(39, 15, 9, '2022-02-18 14:32:10', 'CVDEMO.pdf'),
(41, 17, 7, '2022-02-18 18:01:37', 'CVDEMO.pdf'),
(42, 10, 8, '2022-02-18 18:03:07', 'CVDEMO.pdf'),
(43, 13, 10, '2022-02-18 18:07:57', 'CVDEMO.pdf'),
(44, 15, 10, '2022-02-18 18:08:21', 'CVDEMO.pdf'),
(45, 17, 3, '2022-02-18 18:09:38', 'MyCVDemo.pdf'),
(46, 13, 3, '2022-02-18 18:10:04', 'MyCVDemo.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `loai_nguoidung`
--

CREATE TABLE IF NOT EXISTS `loai_nguoidung` (
  `id` int(10) NOT NULL,
  `nguoi_dung` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loai_nguoidung`
--

INSERT INTO `loai_nguoidung` (`id`, `nguoi_dung`) VALUES
(1, 'Admin'),
(2, 'Nhà tuyển dụng'),
(3, 'Sinh viên');

-- --------------------------------------------------------

--
-- Table structure for table `nguoi_dung`
--

CREATE TABLE IF NOT EXISTS `nguoi_dung` (
  `ma_nd` int(10) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `vai_tro` int(10) NOT NULL,
  `hinh_anh` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nguoi_dung`
--

INSERT INTO `nguoi_dung` (`ma_nd`, `email`, `mat_khau`, `vai_tro`, `hinh_anh`) VALUES
(1, 'khanhthi@gmail.com', '123', 1, 'logo.png'),
(3, 'ngoinhahanhphucthi@gmail.com', '123', 3, 'h1.jpg'),
(9, 'nhatanh@gmail.com', '123', 2, 'h1.jpg'),
(10, 'nhi@gmail.com', '123', 2, 'logo.png'),
(11, 'my@gmail.com', '12345', 3, 'h1.jpg'),
(12, 'nhi@gmail.com', ' 123 ', 2, 'logo.png'),
(22, 'tuan@gmail.com', '123456', 3, 'logo.png'),
(29, 'pha@gmail.com', '123', 3, 'logo.png'),
(30, 'dai@gmail.com', '123', 3, 'logo.png'),
(44, 'phong@gmail.com', '123', 2, 'logo.png'),
(45, 'khoa@gmail.com', '123', 2, 'logo.png'),
(47, 'phong@gmail.com', '123', 3, NULL),
(48, 'thu@gmail.com', '123', 2, NULL),
(49, 'phuc@gmail.com', '123', 3, NULL),
(50, 'test@gmail.com', '123', 2, NULL),
(51, 'test1@gmail.com', '123', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nhan_vien`
--

CREATE TABLE IF NOT EXISTS `nhan_vien` (
  `ma_nhanvien` int(10) NOT NULL,
  `ho_ten` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_sinh` date NOT NULL,
  `gioi_tinh` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CCCD` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `so_dien_thoai` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `chuc_vu` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ma_congty` int(10) NOT NULL,
  `ma_taikhoan` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nhan_vien`
--

INSERT INTO `nhan_vien` (`ma_nhanvien`, `ho_ten`, `ngay_sinh`, `gioi_tinh`, `CCCD`, `so_dien_thoai`, `chuc_vu`, `ma_congty`, `ma_taikhoan`) VALUES
(9, 'Nhat Anh', '2000-07-22', 'Nam', '12545487000', '+84 (236) 3 958 433', 'Nhân viên tuyển dụng', 6, 9),
(10, 'Nhi Đoàn', '2000-07-22', 'Nữ', '12545487000', '+84 (236) 3 958 555', NULL, 7, 10),
(11, 'Nguyễn Hằng Nhi', '2000-07-11', 'Nữ', '049986775623', '0947888333', 'Quản lý tuyển dụng', 8, 12),
(12, ' Hoàng Khánh Phong', '2000-02-16', 'Nam', '049878787887', '0918376763', 'Trưởng phòng tuyển dụng', 18, 44),
(13, ' Phan Anh Khoa', '2022-02-10', 'Nam', '049878553784', ' 0782764523', 'test', 19, 45),
(14, 'Lê Anh Thư', '2000-07-04', 'Nữ', '049986435626', '0918454379', NULL, 15, 48);

-- --------------------------------------------------------

--
-- Table structure for table `nha_tuyen_dung`
--

CREATE TABLE IF NOT EXISTS `nha_tuyen_dung` (
  `ma_congty` int(10) NOT NULL,
  `ten_congty` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `quy_mo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `website` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nha_tuyen_dung`
--

INSERT INTO `nha_tuyen_dung` (`ma_congty`, `ten_congty`, `dia_chi`, `quy_mo`, `website`) VALUES
(6, 'FPT Software Danang', 'FPT Complex, Khu đô thị FPT City, Ngũ Hành Sơn, Đà Nẵng 550000', 'Hơn 1000 nhân viên', 'https://www.fpt-software.com/our-campus/danang/'),
(7, 'Blue Otter Việt Nam', '153 Đống Đa, Thạch Thang, Hải Châu, Đà Nẵng 550000', 'Hơn 500 nhân viên', 'https://www.facebook.com/blueottervn/'),
(8, 'Công Ty Phần Mềm Enclave Đà Nẵng', '453 – 455 Hoàng Diệu, Bình Thuận, Q. Hải Châu, TP. Đà Nẵng', '500 nhân viên', ' enclaveit.com'),
(10, 'GMO-Z.COM RUNSYSTEM ĐÀ NẴNG', 'tầng 1, tòa nhà Công ty trực thăng Miền Trung, đường Nguyễn Văn Linh, phường Thạc Gián, quận Thanh Khê, TP. Đà Nẵng', '1000 nhân viên', 'https://runsystem.net/vi/2018/07/28/chi-nhanh-da-nang-toi-van-phong-moi-long-vui-phoi-phoi/'),
(11, '\r\nCÔNG TY TNHH MONSTAR LAB VIỆT NAM', 'Lô 35, đường số 4, khu công nghiệp An Đồn, phường An Hải Bắc, Quận Sơn Trà, Đà Nẵng', 'Hơn 500 Nhân viên', 'https://monstar-lab.com/vn/'),
(12, 'Công Ty CP Phần Mềm BRAVO Đà Nẵng', '466 Nguyễn Hữu Thọ, Khuê Trung, Cẩm Lệ, Đà Nẵng ', 'Khoảng 500 nhân viên', 'bravo.com.vn'),
(13, 'Công Ty Cổ Phần Phần Mềm Softech Đà Nẵng', 'Tòa nhà VNPT, 38 Yên Bái, Quận Hải Châu, Đà Nẵng', 'Khoảng 500 nv', 'softech.vn'),
(14, 'Công Ty Phần Mềm Chất Lượng FAST Đà Nẵng', '59B Lê Lợi, Quận Hải Châu, TP. Đà Nẵng', 'Gần 1000 nv', 'fast.com.vn'),
(15, 'Công Ty Cổ Phần Phát Triển Phần Mềm ASIA Đà Nẵng', ' 482 Trưng Nữ Vương, Quận Hải Châu, TP. Đà Nẵng', 'Hơn 500 nv', 'asiasoft.com.vn'),
(18, 'Công Ty Phần Mềm Trí Tuệ Đà Nẵng', '76 Bạch Đằng, Hải Châu 1, Hải Châu, Đà Nẵng\n\n\n', 'Hơn 500 Nhân viên', 'ttt.com.vn'),
(19, 'Công ty TNHH Minawork - Công Ty Phần Mềm Ứng Dụng Đà Nẵng', '3F 25 Đường 2/9, Q. Hải Châu,Tp. Đà Nẵng', 'Gần 500 nhân viên', 'minaworks.vn '),
(20, 'Công Ty Cổ Phần Đầu Tư Và Công Nghệ BAP - Công Ty Phần Mềm Chất Lượng Đà Nẵng', '81 Quang Trung, Quận Hải Châu, TP. Đà Nẵng', 'Khoảng 500 nhân viên', 'bap-software.net'),
(21, 'CÔNG TY CỔ PHẦN THƯƠNG MẠI DỊCH VỤ THCMEDIA', 'Quận Ngũ Hành Sơn\r\n,\r\nĐà Nẵng', 'Hơn 500 nhân viên', 'thmda.com.vn');

-- --------------------------------------------------------

--
-- Table structure for table `sinh_vien`
--

CREATE TABLE IF NOT EXISTS `sinh_vien` (
  `ma_sinhvien` int(10) NOT NULL,
  `ho_ten` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `gioi_tinh` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cccd` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_sinh` date NOT NULL,
  `so_dien_thoai` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `trinh_do` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mo_ta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ma_truonghoc` int(10) NOT NULL,
  `ma_taikhoan` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sinh_vien`
--

INSERT INTO `sinh_vien` (`ma_sinhvien`, `ho_ten`, `dia_chi`, `gioi_tinh`, `cccd`, `ngay_sinh`, `so_dien_thoai`, `trinh_do`, `mo_ta`, `ma_truonghoc`, `ma_taikhoan`) VALUES
(2, 'Doan Thi Thuy Linh', '30 Nguyển Văn Linh, Đà Nẵng', 'Nữ', '049457362891', '2000-07-22', '0947664887', 'Sinh viên năm cuối', 'Có kỹ năng về java, sql, javascript, html ,css,...', 1, 3),
(3, 'Tran Thien My', '127 Ngô Quyền, Đà Nẵng', 'Nam', '049938657463', '2001-04-18', '0988667676', 'Sinh viên năm 3', 'Có kỹ năng về php, mysql, reactjs,...', 7, 11),
(6, 'Bui Anh Tuan', '95 Phan Bội Châu, Đà Năng', 'Nam', '049746321185', '2000-02-09', ' 0986746352', 'Sinh viên năm cuối', ' Có kỹ nẵng về C#, sql,...', 4, 22),
(7, 'Hoang Phan My Pha', ' Phú Lộc, Huế', 'Nữ', '049857462231', '2000-04-21', ' 0916847563', 'Sinh viên năm cuối', 'Có kỹ năng về java, sql,...', 1, 29),
(8, 'Tran Quang Dai', '318 Hà Huy Tập, Đà Nẵng', 'Nam', ' 049875647834', '2001-02-02', ' 0987589948', 'Sinh viên năm 3', 'Có kỹ năng về php, mysql, reactjs,...', 3, 30),
(9, 'Huynh An Phong', '328 Phan Thanh, Đà Nẵng', 'Nam', '04985746637', '2000-09-15', '0918454365', 'Sinh viên năm cuối', 'Có kiến thức về php,mysql....', 13, 47),
(10, 'Trần Thị Thanh Phúc', 'Hải Phòng, Đà Nẵng', 'Nữ', '04985746555', '2000-07-09', '0918454366', 'Sinh viên năm cuối', NULL, 4, 49);

-- --------------------------------------------------------

--
-- Table structure for table `tin_tuyen_dung`
--

CREATE TABLE IF NOT EXISTS `tin_tuyen_dung` (
  `ma_tuyendung` int(10) NOT NULL,
  `ma_vitri` int(10) NOT NULL,
  `ma_nhanvien` int(10) NOT NULL,
  `tieu_de` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `so_luong` int(10) NOT NULL,
  `muc_luong` decimal(10,0) NOT NULL,
  `mo_ta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trinh_do` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `gioi_tinh` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hinh_thuc_lam_viec` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quyen_loi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tu_khoa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_dang` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tin_tuyen_dung`
--

INSERT INTO `tin_tuyen_dung` (`ma_tuyendung`, `ma_vitri`, `ma_nhanvien`, `tieu_de`, `so_luong`, `muc_luong`, `mo_ta`, `trinh_do`, `gioi_tinh`, `hinh_thuc_lam_viec`, `quyen_loi`, `tu_khoa`, `ngay_dang`) VALUES
(10, 10, 9, 'Thực Tập Sinh Tester', 5, '2500000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', 'Sinh viên năm cuối/mới tốt nghiệp ở các ngành Khoa học máy tính, phần mềm tại các trường Cao dẳng, đại học', ' Nam/Nữ', ' Fulltime/Partime', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', 'tester, TESTER', '2022-01-02 00:00:00'),
(11, 8, 10, ' Tuyển dụng thực tập sinh java', 4, '2000000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', 'Sinh viên năm cuối/mới tốt nghiệp ở các ngành Khoa học máy tính, phần mềm tại các trường Cao dẳng, đại học', ' nam/nữ', ' Fulltime', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', ' java', '2022-01-01 00:00:00'),
(12, 8, 13, 'Tuyển dụng tts java', 2, '1500000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', '  Năm cuối đại học, cao đẳng ngành cntt,khmt...', '  nam/nữ', ' remote', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', 'java', '2022-01-11 06:43:01'),
(13, 6, 9, 'Tuyển dụng tts C#', 10, '1500000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', ' Năm cuối đại học, cao đẳng ngành cntt,khmt...', ' nam/nữ', 'remote', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', ' c#,csharp', '2022-01-11 06:52:45'),
(14, 4, 9, 'Tuyển dụng thực tập sinh Reactjs', 8, '1500000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', ' Năm cuối đại học, cao đẳng ngành cntt,khmt...', 'Nam/Nữ', 'Fulltime/Partime', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', ' reactjs', '2022-01-12 07:34:55'),
(15, 7, 9, 'Tuyển dụng tts python', 5, '2000000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', ' Năm cuối đại học, cao đẳng ngành cntt,khmt...', 'Nam/Nữ', 'Fulltime', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', 'python', '2022-01-12 07:36:27'),
(16, 11, 13, 'Tuyển dụng thực tập sinh javascript', 10, '2000000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', 'Sinh viên năm cuối/mới tốt nghiệp ở các ngành Khoa học máy tính, phần mềm tại các trường Cao dẳng, đại học', 'Nam/Nữ', 'Remote', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', 'javascript', '2022-02-16 00:00:00'),
(17, 11, 9, 'Tuyển dụng thực tập sinh javascript,html,css', 3, '2000000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', 'Sinh viên năm cuối/mới tốt nghiệp ở các ngành Khoa học máy tính, phần mềm tại các trường Cao dẳng, đại học', 'Nam/Nữ', 'Fulltime/Partime', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', 'javascript', '2022-02-16 00:00:00'),
(19, 16, 12, 'TUYỂN DỤNG TTS .NET', 2, '2000000', 'Được đào tạo cơ bản đến nâng cao cả chuyên môn, nghiệp vụ testing & Quy trình làm việc: Waterfall, Agile.\nNắm bắt nghiệp vụ đã phân tích để viết checklist/testcases hoặc phát triển hệ thống automation test trên nền web/mobile cùng với các ', 'Sinh viên năm cuối/mới tốt nghiệp ở các ngành Khoa học máy tính, phần mềm tại các trường Cao dẳng, đại học', 'Nam/Nữ', 'Fulltime', 'Thực tập có lương với thời gian linh động theo lịch học dành cho sinh viên năm cuối chuyên ngành Công nghệ thông tin, phần mềm...(có thể vừa học vừa làm).\nKý hợp đồng lao động chính thức và tham gia BHXH sau thời gian thực tập.\nThời gian làm việc: 8:00 ', '.Net', '2022-02-16 00:00:00'),
(20, 1, 9, ' Tuyển dụng thực tập sinh PHP', 5, '2000000', 'test', ' Sinh viên năm cuối, năm 3 các trường đại học, cao đẳng...', ' Nam/Nữ', ' Fulltime/Partime', 'test', '  php', '2022-02-18 06:40:14'),
(21, 8, 9, 'Tuyển dụng thực tập sinhJava', 5, '2000000', 'demo', 'demo', 'Nam/Nữ', 'Fulltime/Partime', 'demo', ' java', '2022-02-19 02:15:11');

-- --------------------------------------------------------

--
-- Table structure for table `truong_hoc`
--

CREATE TABLE IF NOT EXISTS `truong_hoc` (
  `ma_truonghoc` int(10) NOT NULL,
  `ten_truong` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `truong_hoc`
--

INSERT INTO `truong_hoc` (`ma_truonghoc`, `ten_truong`) VALUES
(1, 'UTE-Trường Đại học Sư phạm Kỹ thuật - Đại học Đà Nẵng'),
(2, 'DUT-Trường ĐH Bách Khoa - Đại học Đà Nẵng'),
(3, 'FPT-Đại học FPT '),
(4, 'QSC-Đại học Công nghệ Thông tin – Đại học Quốc gia TP.HCM'),
(5, 'QSB-Đại học Bách khoa TP.HCM'),
(6, 'QST-Đại học Khoa học tự nhiên – Đại học Quốc gia TP.HCM'),
(7, 'BVS-Học viện Công nghệ Bưu chính Viễn thông (Phía Nam)'),
(8, 'BKA-Đại học Bách Khoa Hà Nội'),
(10, 'DHT-Đại học Khoa học Huế'),
(11, 'DTC-Đại học Công Nghệ Thông Tin và Truyền Thông – Đại Học Thái Nguyên '),
(12, 'QHT-Đại học Khoa học tự nhiên-Đại học Quốc gia Hà Nội'),
(13, 'BVH-Học viện Công nghệ Bưu chính Viễn thông (Phía Bắc)'),
(14, 'DDT-Đại học Duy Tân'),
(15, 'DCN-Đại học Công nghiệp Hà Nội'),
(16, 'HHA-Đại học Hàng Hải Việt Nam');

-- --------------------------------------------------------

--
-- Table structure for table `vi_tri_tuyen_dung`
--

CREATE TABLE IF NOT EXISTS `vi_tri_tuyen_dung` (
  `ma_vitri` int(10) NOT NULL,
  `vi_tri_tuyen_dung` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vi_tri_tuyen_dung`
--

INSERT INTO `vi_tri_tuyen_dung` (`ma_vitri`, `vi_tri_tuyen_dung`) VALUES
(1, 'PHP'),
(4, 'Reactjs'),
(6, 'C# '),
(7, 'Python'),
(8, 'Java '),
(10, 'Tester'),
(11, 'Javascript'),
(12, 'Front-end'),
(13, 'Vuejs'),
(14, 'React Native'),
(15, 'Nodejs'),
(16, '.Net'),
(17, 'C,C++'),
(18, 'Ruby');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ho_so_ung_tuyen`
--
ALTER TABLE `ho_so_ung_tuyen`
  ADD PRIMARY KEY (`ma_hoso`),
  ADD KEY `ma_tuyendung` (`ma_tuyendung`),
  ADD KEY `ma_sinhvien` (`ma_sinhvien`);

--
-- Indexes for table `loai_nguoidung`
--
ALTER TABLE `loai_nguoidung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD PRIMARY KEY (`ma_nd`),
  ADD KEY `vai_tro` (`vai_tro`);

--
-- Indexes for table `nhan_vien`
--
ALTER TABLE `nhan_vien`
  ADD PRIMARY KEY (`ma_nhanvien`),
  ADD KEY `ma_congty` (`ma_congty`),
  ADD KEY `ma_taikhoan` (`ma_taikhoan`);

--
-- Indexes for table `nha_tuyen_dung`
--
ALTER TABLE `nha_tuyen_dung`
  ADD PRIMARY KEY (`ma_congty`);

--
-- Indexes for table `sinh_vien`
--
ALTER TABLE `sinh_vien`
  ADD PRIMARY KEY (`ma_sinhvien`),
  ADD KEY `ma_truonghoc` (`ma_truonghoc`),
  ADD KEY `ma_taikhoan` (`ma_taikhoan`);

--
-- Indexes for table `tin_tuyen_dung`
--
ALTER TABLE `tin_tuyen_dung`
  ADD PRIMARY KEY (`ma_tuyendung`),
  ADD KEY `ma_congty` (`ma_vitri`),
  ADD KEY `ma_nhanvien` (`ma_nhanvien`);

--
-- Indexes for table `truong_hoc`
--
ALTER TABLE `truong_hoc`
  ADD PRIMARY KEY (`ma_truonghoc`);

--
-- Indexes for table `vi_tri_tuyen_dung`
--
ALTER TABLE `vi_tri_tuyen_dung`
  ADD PRIMARY KEY (`ma_vitri`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ho_so_ung_tuyen`
--
ALTER TABLE `ho_so_ung_tuyen`
  MODIFY `ma_hoso` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  MODIFY `ma_nd` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `nhan_vien`
--
ALTER TABLE `nhan_vien`
  MODIFY `ma_nhanvien` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `nha_tuyen_dung`
--
ALTER TABLE `nha_tuyen_dung`
  MODIFY `ma_congty` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `sinh_vien`
--
ALTER TABLE `sinh_vien`
  MODIFY `ma_sinhvien` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tin_tuyen_dung`
--
ALTER TABLE `tin_tuyen_dung`
  MODIFY `ma_tuyendung` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `truong_hoc`
--
ALTER TABLE `truong_hoc`
  MODIFY `ma_truonghoc` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `vi_tri_tuyen_dung`
--
ALTER TABLE `vi_tri_tuyen_dung`
  MODIFY `ma_vitri` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ho_so_ung_tuyen`
--
ALTER TABLE `ho_so_ung_tuyen`
  ADD CONSTRAINT `ho_so_ung_tuyen_ibfk_1` FOREIGN KEY (`ma_tuyendung`) REFERENCES `tin_tuyen_dung` (`ma_tuyendung`),
  ADD CONSTRAINT `ho_so_ung_tuyen_ibfk_2` FOREIGN KEY (`ma_sinhvien`) REFERENCES `sinh_vien` (`ma_sinhvien`);

--
-- Constraints for table `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD CONSTRAINT `nguoi_dung_ibfk_1` FOREIGN KEY (`vai_tro`) REFERENCES `loai_nguoidung` (`id`);

--
-- Constraints for table `nhan_vien`
--
ALTER TABLE `nhan_vien`
  ADD CONSTRAINT `nhan_vien_ibfk_4` FOREIGN KEY (`ma_congty`) REFERENCES `nha_tuyen_dung` (`ma_congty`),
  ADD CONSTRAINT `nhan_vien_ibfk_5` FOREIGN KEY (`ma_taikhoan`) REFERENCES `nguoi_dung` (`ma_nd`);

--
-- Constraints for table `sinh_vien`
--
ALTER TABLE `sinh_vien`
  ADD CONSTRAINT `sinh_vien_ibfk_1` FOREIGN KEY (`ma_truonghoc`) REFERENCES `truong_hoc` (`ma_truonghoc`),
  ADD CONSTRAINT `sinh_vien_ibfk_2` FOREIGN KEY (`ma_taikhoan`) REFERENCES `nguoi_dung` (`ma_nd`);

--
-- Constraints for table `tin_tuyen_dung`
--
ALTER TABLE `tin_tuyen_dung`
  ADD CONSTRAINT `tin_tuyen_dung_ibfk_2` FOREIGN KEY (`ma_vitri`) REFERENCES `vi_tri_tuyen_dung` (`ma_vitri`),
  ADD CONSTRAINT `tin_tuyen_dung_ibfk_3` FOREIGN KEY (`ma_nhanvien`) REFERENCES `nhan_vien` (`ma_nhanvien`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
