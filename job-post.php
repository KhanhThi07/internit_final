<?php
    session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Đăng nhập</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center" style="background-image:linear-gradient(to bottom,rgba(255, 99, 71, 0) 0%,rgba(255, 99, 71, 0) 100%), url('https://images.pexels.com/photos/1229861/pexels-photo-1229861.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'); ">
    <form class="form-signin" style="background-color: #fff;border: 3px solid #00bcd4 ;" action="job-post.php" method="post">
      <img class="mb-4" src="images/logo.png" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Sinh viên </h1>
      <label for="inputEmail" class="sr-only">Địa chỉ email</label>
      <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Địa chỉ email đăng nhập" required autofocus>
      <label for="inputPassword" class="sr-only">Mật khẩu</label>
      <input type="password" id="inputPassword" name="mat_khau" class="form-control" placeholder="Mật khẩu" required>
      <!-- <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div> -->
      <input class="btn btn-lg btn-primary btn-block" value="Đăng nhập" style="background-color: #00bcd4; border-color: #00bcd4;" type="submit" name="submit1" placeholder="Đăng nhập">
      <a href="sign_up.php" style="color:  #00bcd4;">Tạo một tài khoản</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2021-2022</p>
    </form>

    <form class="form-signin" style="background-color: #fff;border: 3px solid #00bcd4 ;" action="job-post.php" method="post">
      <img class="mb-4" src="images/logo.png" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Nhà tuyển dụng </h1>
      <label for="inputEmail" class="sr-only">Địa chỉ email</label>
      <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Địa chỉ email đăng nhập" required autofocus>
      <label for="inputPassword" class="sr-only">Mật khẩu</label>
      <input type="password" id="inputPassword" name="mat_khau" class="form-control" placeholder="Mật khẩu" required>
      <!-- <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div> -->
      <input class="btn btn-lg btn-primary btn-block" style="background-color: #00bcd4; border-color: #00bcd4;" type="submit" value="Đăng nhập" name="submit2" placeholder="Đăng nhập">
      <a href="sign_up_nhatuyendung.php" style="color:  #00bcd4;">Tạo một tài khoản</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2021-2022</p>
    </form>
  </body>
</html>


<?php
include('connection/db.php');
  if (isset($_POST['submit1'])) {
     $email=$_POST['email'];
     $pass=$_POST['mat_khau'];

    $query=mysqli_query($conn,"select * from nguoi_dung nd where email = '$email' and mat_khau='$pass' and vai_tro='3' ");

    if($query)
    {
    if(mysqli_num_rows($query)>0){
      $_SESSION['email']=$email;
      header('location:index.php');
      exit();
    }else{
      echo "<script>alert('Email or password is incorrect. Please try again!')</script>";
    }
  }
  }
?>

<?php
include('connection/db.php');
  if (isset($_POST['submit2'])) {
     $email=$_POST['email'];
     $pass=$_POST['mat_khau'];

    $query=mysqli_query($conn,"select * from nguoi_dung where email = '$email' and mat_khau='$pass' and vai_tro='2' " );

    if($query)
    {
      if(mysqli_num_rows($query)>0){
        $_SESSION['email']=$email;
        header('location:http://localhost/final_intern_it/admin/admin_dashboard.php');
        exit();
      
          }else{
             echo "<script>alert('Email or password is incorrect. Please try again!')</script>";

      }
    }
    

  
}  
?>