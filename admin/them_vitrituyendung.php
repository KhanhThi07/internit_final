<?php
  include('include/header.php');
  include('include/sidebar.php');
?>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="admin_dashboard.php">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="vi_tri_tuyen_dung.php">Vị trí tuyển dụng</a></li>
    <li class="breadcrumb-item"><a href="#">Thêm vị trí tuyển dụng</a></li>
  </ol>
</nav>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 class="h2">Thêm vị trí tuyển dụng</h1>
  
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
               
              </div>
             <!-- <a class="btn btn-primary" href="them_nguoidung.php">Thêm người dùng</a> -->
            </div>
          </div>
          <div style="width: 60%; margin-left:20%; background-color: #F2F4F4;">
          <div id="msg"></div>
              <form action="" method="POST" style="margin: 3%; padding: 3%;" name="vi_tri_tuyen_dung_form" id="vi_tri_tuyen_dung_form">
                  <div class="form-group">
                      <label for="Ten nha tuyen dung">Vị trí tuyển dụng</label>
                      <input type="text" name="vi_tri_tuyen_dung" id="ten_congty" class="form-control" placeholder="Nhập vị trí tuyến dụng">
                  </div>
                  <!-- <div class="form-group">
                      <label for="Dia chi nha tuyen dung">Địa chỉ</label>
                      <input type="text" name="dia_chi" id="dia_chi" class="form-control" placeholder="Nhập địa chỉ">
                  </div> -->
                
                  <!-- <div class="form-group">
                      <label for="Dia chi website">Website</label>
                      <textarea name="website" id="website" class="form-control" placeholder="Địa chỉ website" cols="30" rows="3"></textarea>
                      <input type="text" name="website" id="website" value="<?php echo  $website; ?>"  class="form-control" placeholder="Nhập địa chỉ website">
                     
                  </div> -->


                 

                  <div class="form-group" >
                         <input type="submit" class="btn btn-block btn-success" value="Thêm" placeholder="Save" name="submit" id="submit">

                     </div>
                  <!-- <div class="form-group">
                      <label for="Ảnh người dùng">Hình ảnh</label>
                      <input type="text" class="form-control" placeholder="Hình ảnh">
                  </div> -->

              </form>

          </div>

         

          <canvas class="my-4" id="myChart" width="900" height="380"></canvas>          
          <div class="table-responsive">
          
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    
<!-- datatables plugin -->
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   
   <script>
       $(document).ready(function() {
    $('#example').DataTable();
} );
   </script>

   <script>
       $(document).ready(function(){
           $("#submit").click(function(){
               var vi_tri_tuyen_dung = $("#vi_tri_tuyen_dung").val();
            //    var dia_chi = $("#dia_chi").val();
              

               var data = $('#vi_tri_tuyen_dung_form').serialize();

               if (vi_tri_tuyen_dung=='') {
                   alert("Vui lòng nhập dữ liệu!!!");
                   return false;
               }

               $.ajax({
                   type:"POST",
                   url:  "vitrituyendung_them.php",
                   data: data,
                   success : function(data){
                       $("#msg").html(data);
                    
                   }

               });
           });               
       });

   </script>

  </body>
</html>
