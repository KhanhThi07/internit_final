<?php     
    include('include/header.php');
    include('include/sidebar.php');
    
?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="admin_dashboard.php">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="ho_so_ung_tuyen.php">Hồ sơ ứng tuyển</a></li>
  </ol>
</nav>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 class="h2">Chi tiết hồ sơ ứng tuyển</h1>
  
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
               
              </div>
             <!-- <a class="btn btn-primary" href="them_tintuyendung.php">Tạo tin tuyển dụng</a> -->
            </div>
          </div>    
           
          <form action="" style="border: 1px solid gray; width:80%; margin-left:10%; padding:10px; ">
        <?php
            include('connection/db.php');     
            
            $id=$_GET['id'];
            $sql =mysqli_query($conn,"select * from ho_so_ung_tuyen 
                                    LEFT JOIN tin_tuyen_dung ON ho_so_ung_tuyen.ma_tuyendung= tin_tuyen_dung.ma_tuyendung  
                                    LEFT JOIN nhan_vien ON tin_tuyen_dung.ma_nhanvien= nhan_vien.ma_nhanvien   

                                    LEFT JOIN sinh_vien ON ho_so_ung_tuyen.ma_sinhvien= sinh_vien.ma_sinhvien                                                                                
                                    LEFT JOIN truong_hoc ON sinh_vien.ma_truonghoc= truong_hoc.ma_truonghoc   

                                    LEFT JOIN nguoi_dung ON nhan_vien.ma_taikhoan= nguoi_dung.ma_nd
                                    WHERE ma_hoso = '$id'                                                                     

                           ");     
                             
                              $rowcount=mysqli_num_rows($sql);
                for($i=1;$i<=$rowcount;$i++)
             {
              $row=mysqli_fetch_array($sql);            
             ?>

            <div class="form-group">       
                <label for="">Tiêu đề</label>                       
                <td><?php echo $row['tieu_de'] ?></td>
            </div>  
           
            <div class="form-group">       
                <label for="">Họ tên</label>                       
                <td><?php echo $row['ho_ten'] ?></td>
            </div>
           
            <div class="form-group">       
                <label for="">Trình độ</label>                       
                <td><?php echo $row['trinh_do'] ?></td>
            </div>

            <div class="form-group">       
                <label for="">Tên trường</label>                       
                <td><?php echo $row['ten_truong'] ?></td>
            </div>

            <div class="form-group">       
                <label for="">Số điện thoại</label>                       
                <td><?php echo $row['so_dien_thoai'] ?></td>
            </div>

            <div class="form-group">       
                <label for="">Ngày nộp</label>                       
                <td><?php echo $row['ngay_nop'] ?></td>            
            </div>

            <div class="form-group">       
                <label for="">File CV</label>                       
                <td><a href="http://localhost/final_intern_IT/files/<?php echo $row['file_cv']; ?>">Tải file</a></td>            
            </div>
          


             
                              
            <?php  } ?>    
                <a href="gui_mail.php?id=<?php echo $id; ?>" class="btn btn-success">Chấp nhận</a>
                <a href="tuchoi_cv.php?id=<?php echo $id; ?>" class="btn btn-danger">Từ chối</a>
            </form>
       

          <canvas class="my-4" id="myChart" width="900" height="380"></canvas>

          
          <div class="table-responsive">
          
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    
<!-- datatables plugin -->
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   
   <script>
       $(document).ready(function() {
    $('#example').DataTable();
} );
   </script>
  </body>
</html>
