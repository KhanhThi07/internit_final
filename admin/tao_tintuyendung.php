<?php          
    include('connection/db.php');
    include('include/header.php');
    include('include/sidebar.php');
?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="admin_dashboard.php">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="#">Tin tuyển dụng</a></li>
  </ol>
</nav>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 class="h2">Danh sách tin tuyển dụng</h1>
  
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
               
              </div>
             <a class="btn btn-primary" href="them_tintuyendung.php">Tạo tin tuyển dụng</a>
            </div>
          </div>

          <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>STT</th>   
                <!-- <th>Nhân viên</th> -->
                <th>Tiêu đề</th>
                <th>Số lượng</th>
                <th>Mức lương</th>
                <th>Vị trí tuyển dụng</th>
                <th>Trình độ</th>
                <!-- <th>Giới tính</th> -->
                <th>Hình thức làm việc </th>
                <th>Quyền lợi</th>
                <!-- <th>Mô tả</th> -->
                <!-- <th>Từ khóa</th> -->
                <th>Ngày đăng</th>
                <th>Thao tác</th>

            </tr>
        </thead>
        <tbody>
           
        <?php
        //  session_start(); 
            include('connection/db.php');
            $a=1;
          // $user = $_SESSION['email'];
          // $query1 =mysqli_query($conn,"select * from nguoi_dung where 
          //       email ='$user");
          // $row1=mysqli_fetch_array($query1);
          // $id = $row1['ma_nd'];

            // $query = mysqli_query($conn,"select ttd.*, vttd.vi_tri_tuyen_dung ,nd.email
            //             from tin_tuyen_dung ttd, vi_tri_tuyen_dung vttd, nhan_vien nv, nguoi_dung nd
            //             where vttd.ma_vitri=ttd.ma_vitri and ttd.ma_nhanvien=nv.ma_nhanvien and nv.ma_taikhoan=nd.ma_nd
            //          and email_nhanvien='{$_SESSION['email']}'");                        
            //              $rowcount=mysqli_num_rows($query);

            $query =mysqli_query($conn,"select * from tin_tuyen_dung ttd 
                                        LEFT JOIN nhan_vien nv ON ttd.ma_nhanvien=nv.ma_nhanvien
                                        LEFT JOIN nguoi_dung nd ON nv.ma_taikhoan=nd.ma_nd
                                        LEFT JOIN vi_tri_tuyen_dung vttd ON ttd.ma_vitri=vttd.ma_vitri
                                        WHERE email='{$_SESSION['email']}' ");

                                        $rowcount=mysqli_num_rows($query);
          // $query = mysqli_query($conn,"select ttd.*, vttd.vi_tri_tuyen_dung 
          //               from tin_tuyen_dung ttd, vi_tri_tuyen_dung vttd, nhan_vien nv, nguoi_dung nd
          //               where vttd.ma_vitri=ttd.ma_vitri and ttd.ma_nhanvien=nv.ma_nhanvien 
          //               and nv.ma_taikhoan=nd.ma_nd and email_nhanvien='{$_SESSION['email']}'
          //                ");          
            // while($rowcount = mysqli_fetch_array($query)){     
            for($i=1;$i<=$rowcount;$i++)
            {
              $row=mysqli_fetch_array($query);
             
            
        ?>
            <tr>
                <td><?php echo $a; ?></td>                
              
                <!-- <td><?php echo $row['email_nhanvien'] ?></td>                 -->
                <td><?php echo $row['tieu_de'] ?></td>
                <td><?php echo $row['so_luong'] ?></td>
                <td><?php echo $row['muc_luong'] ?></td>
                <td><?php echo $row['vi_tri_tuyen_dung'] ?></td>
                <td><?php echo $row['trinh_do'] ?></td>
                <!-- <td><?php echo $row['gioi_tinh'] ?></td> -->
                <td><?php echo $row['hinh_thuc_lam_viec'] ?></td>
                <td><?php echo $row['quyen_loi'] ?></td>
                <!-- <td><?php echo $row['mo_ta'] ?></td> -->
                <!-- <td><?php echo $row['tu_khoa'] ?></td> -->
                <td><?php echo $row['ngay_dang'] ?></td>

                <td>
                  <div class="row">
                    <div class="btn-group">
                      <a href="tintuyendung_sua.php?edit=<?php echo $row['ma_tuyendung'];?>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span></a>
                      <a href="tintuyendung_xoa.php?del=<?php echo $row['ma_tuyendung'];?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>

                    </div>

                  </div>
                </td>
             
                              
            </tr>
            <?php $a++; } ?>    
         
              
        </tbody>
        <tfoot>
            <tr>
            <th>STT</th>   
            <!-- <th>Nhân viên</th> -->
            <th>Tiêu đề</th>
            <th>Số lượng</th>
            <th>Mức lương</th>
            <th>Vị trí tuyển dụng</th>
            <th>Trình độ</th>
            <!-- <th>Giới tính</th> -->
            <th>Hình thức làm việc </th>
            <th>Quyền lợi</th>          
            <!-- <th>Mô tả</th> -->
            <!-- <th>Từ khóa</th> -->
            <th>Ngày đăng</th>
            <th>Thao tác</th>


                
            </tr>
        </tfoot>
    </table>

          <canvas class="my-4" id="myChart" width="900" height="380"></canvas>

          
          <div class="table-responsive">
          
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    
<!-- datatables plugin -->
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   
   <script>
       $(document).ready(function() {
    $('#example').DataTable();
} );
   </script>
   
  </body>
</html>
