<?php  
    include('connection/db.php')   ;
    include('include/header.php');
    include('include/sidebar.php');
    
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
     <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="admin_dashboard.php">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="#">Gửi mail</a></li>
  </ol>
</nav>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 class="h2">Gửi mail</h1>
  
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
               
              </div>
             <!-- <a class="btn btn-primary" href="them_tintuyendung.php">Tạo tin tuyển dụng</a> -->
            </div>
          </div>    
           
          <form action="phpmailer.php" method="post" style="border: 1px solid gray; width:60%; margin-left:10%; padding:10px; ">
        <?php
            include('connection/db.php');     
            
            $id=$_GET['id'];
            $sql =mysqli_query($conn,"select * from ho_so_ung_tuyen 
                                    LEFT JOIN tin_tuyen_dung ON ho_so_ung_tuyen.ma_tuyendung= tin_tuyen_dung.ma_tuyendung  
                                 

                                    LEFT JOIN sinh_vien ON ho_so_ung_tuyen.ma_sinhvien= sinh_vien.ma_sinhvien                                                                                
                                 

                                    LEFT JOIN nguoi_dung ON sinh_vien.ma_taikhoan= nguoi_dung.ma_nd
                                    WHERE ma_hoso = '$id'                                                                     

                           ");     
                             
                              $rowcount=mysqli_num_rows($sql);
                for($i=1;$i<=$rowcount;$i++)
             {
              $row=mysqli_fetch_array($sql);            
             ?>
             <h1><?php echo strtoupper($row['ho_ten']) ?></h1>
             <hr>
             <input type="hidden" name="id" id="id" value="<?php echo $id;?>">

            <div class="form-group">       
                <label for="">Đến :</label>                       
                <td><input type="email" name="to" id="to" class="form-control" value="<?php echo $row['email']?>" ></td>
            </div>  

            <div class="form-group">       
                <label for="">Từ :</label>                       
                <td><input type="email" name="from" id="from" class="form-control" placeholder="From..."></td>
            </div>  

            <div class="form-group">       
                <label for="">Nội dung :</label>                       
                <td><textarea name="body" id="body" class="form-control" cols="30px"rows="10px">Gửi <?php echo
                 strtoupper($row['ho_ten']) ?></textarea></td>
            </div>   
               
           <?php  } ?>  

               <input type="submit" class="btn btn-success btn-block" name="submit" id="submit" value="Gửi">
            </form>
       

          <canvas class="my-4" id="myChart" width="900" height="380"></canvas>
          
          <div class="table-responsive">
          
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    
<!-- datatables plugin -->
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   
   <script>
       $(document).ready(function() {
    $('#example').DataTable();
} );
   </script>
  </body>
</html>
