<?php
    include('connection/db.php');

  include('include/header.php');
  include('include/sidebar.php');
?>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="admin_dashboard.php">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="tao_tintuyendung.php">Tin tuyển dụng</a></li>
    <li class="breadcrumb-item"><a href="#">Thêm tin tuyển dụng</a></li>
  </ol>
</nav>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 class="h2">Thêm tin tuyển dụng</h1>
  
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
               
              </div>
             <!-- <a class="btn btn-primary" href="them_nguoidung.php">Thêm người dùng</a> -->
            </div>
          </div>
          <div style="width: 60%; margin-left:20%; background-color: #F2F4F4;">
          <div id="msg"></div>
              <form action="" method="POST" style="margin: 3%; padding: 3%;" name="tin_tuyen_dung_form" id="tin_tuyen_dung_form">              
              
              <div >                      
               <?php                          
                 include('connection/db.php');
                 $query =mysqli_query($conn,"select * from tin_tuyen_dung ttd
                               LEFT JOIN nhan_vien nv ON ttd.ma_nhanvien=nv.ma_nhanvien
                              LEFT JOIN nguoi_dung nd ON nv.ma_taikhoan=nd.ma_nd
                              LEFT JOIN vi_tri_tuyen_dung vttd ON ttd.ma_vitri=vttd.ma_vitri
                                WHERE email='{$_SESSION['email']}' ");
                                $rowcount=mysqli_num_rows($query);
                                for($i=1;$i<=$rowcount;$i++)
                                {
                                  $row=mysqli_fetch_array($query);                               
                                  ?>
                      <input type="hidden" name="ma_nhanvien" id="ma_nhanvien" value="<?php echo $row['ma_nhanvien'];?>" >
                      <?php  } ?> 
                   
                   
                  </div>    
              
              <div class="form-group">
                      <label for="Tieu de">Tiêu đề</label>
                      <input type="text" name="tieu_de" id="tieu_de" class="form-control" placeholder="Nhập tiêu đề">
                  </div>
                  <div class="form-group">
                      <label for="So luong">Số lượng</label>
                      <input type="text" name="so_luong" id="so_luong" class="form-control" placeholder="Nhập số lượng">
                  </div>
                  <div class="form-group">
                      <label for="Muc luong">Mức lương</label>
                      <input type="text" name="muc_luong" id="muc_luong" class="form-control" placeholder="Nhập mức lương">
                  </div>
                  <div class="form-group">
                  <label for="Vi tri tuyen dung">Vị trí tuyển dụng</label>
                     
                      <!-- <input type="text" name="vi_tri_tuyen_dung" id="vi_tri_tuyen_dung" class="form-control" placeholder="Nhập vị trí tuyển dụng"> -->
                      <select class="form-control" name="vi_tri_tuyen_dung" id="vi_tri_tuyen_dung">             
                          <?php                          
                              include('connection/db.php');

                              $query1 = mysqli_query($conn,"SELECT * FROM vi_tri_tuyen_dung");
                              if (mysqli_num_rows($query1)>0) {
                                while ($vitri = mysqli_fetch_assoc($query1)) {
                            ?>
                                    <option value="<?php echo $vitri['ma_vitri'];?>">
                                        <?php echo $vitri['vi_tri_tuyen_dung'];?>
                                    </option>
                            <?php
                            ob_start();
                                }
                              }         
                              ob_end_flush();    
                            ?>                         
                      </select>              
                    
                  </div>                 
                  <div class="form-group">
                      <label for="Trinh do">Trình độ</label>
                      <input type="text" name="trinh_do" id="trinh_do" class="form-control" placeholder="Nhập trình độ">
                  </div>
                  <div class="form-group">
                      <label for="Gioi tinh">Giới tính</label>
                      <input type="text" name="gioi_tinh" id="gioi_tinh" class="form-control" placeholder="Nhập giới tính">

                      <!-- <select name="gioi_tinh" value=" " class="form-control" id="gioi_tinh">                      
                          <option value="">Nam</option>
                          <option value="">Nữ</option>
                          <option value="">Nam/Nữ</option>
                      </select>                     -->
                  </div>
                  <div class="form-group">
                      <label for="Hinh thuc lam viec">Hình thức làm việc</label>
                      <input type="text" name="hinh_thuc_lam_viec" id="hinh_thuc_lam_viec"  class="form-control" placeholder="Nhập hình thức làm việc">
                   </div>

                   <!-- <div class="form-group">
                   <label for="Hinh thuc lam viec">Tỉnh / Thành phố</label>
                        <select name="calc_shipping_provinces" class="form-control" required="">
                                <option value="">Tỉnh / Thành phố</option>
                        </select>
                   <label for="Hinh thuc lam viec">Quận / Huyện</label>
                        <select name="calc_shipping_district" class="form-control" required="">
                                <option value="">Quận / Huyện</option>
                        </select>
                            <input class="billing_address_1" name="" type="hidden" value="">
                            <input class="billing_address_2" name="" type="hidden" value="">
                   </div> -->                 

                   <div class="form-group">
                      <label for="Quyen loi">Quyền lợi</label>
                      <!-- <input type="text" name="quyen_loi" id="quyen_loi"  class="form-control" placeholder="Nhập quyền lợi"> -->
                      <textarea name="quyen_loi" id="quyen_loi" class="form-control" placeholder="Nhập quyền lợi" cols="30" rows="3"></textarea>

                   </div>
                   <div class="form-group">
                      <label for="Mo ta">Mô tả</label>
                      <textarea name="mo_ta" id="mo_ta" class="form-control" placeholder="Nhập mô tả" cols="30" rows="3"></textarea>
                      <!-- <input type="email" name="email" id="email" class="form-control" placeholder="Nhập email"> -->
                  </div>
                  <div class="form-group">
                      <label for="Tu khoa">Từ khóa</label>                    
                      <input type="text" name="tu_khoa" id="tu_khoa" class="form-control" placeholder="Nhập từ khóa tìm kiếm">
                  </div>                 

                 

                  <div class="form-group" >
                         <input type="submit" class="btn btn-block btn-success" placeholder="Save" value="Thêm" name="submit" id="submit">

                     </div>                 
              </form>
          </div>         

          <canvas class="my-4" id="myChart" width="900" height="380"></canvas>          
          <div class="table-responsive">
          
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    
<!-- datatables plugin -->
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   
   <script>
       $(document).ready(function() {
    $('#example').DataTable();
} );
   </script>

   <script>
       $(document).ready(function(){
           $("#submit").click(function(){
           
               var tieu_de = $("#tieu_de").val();
               var so_luong = $("#so_luong").val();
               var muc_luong = $("#muc_luong").val();
               var vi_tri_tuyen_dung = $("#vi_tri_tuyen_dung").val();
               var trinh_do = $("#so_dien_thoai").val();
               var gioi_tinh = $("#gioi_tinh").val();
               var hinh_thuc_lam_viec = $("#hinh_thuc_lam_viec").val();
               var quyen_loi = $("#quyen_loi").val();
               var mo_ta = $("#mo_ta").val();
               var tu_khoa = $("#tu_khoa").val();

               var data = $('#tin_tuyen_dung_form').serialize();

               if (tieu_de==''||so_luong==''||muc_luong==''||vi_tri_tuyen_dung==''||trinh_do==''||gioi_tinh==''
               ||hinh_thuc_lam_viec==''||quyen_loi==''||mo_ta=='') {
                   alert("Vui lòng nhập dữ liệu!!!");
                   return false;
               }

               $.ajax({
                   type:"POST",
                   url:  "tintuyendung_them.php",
                   data: data,
                   success : function(data){
                       $("#msg").html(data);
                      // alert(data);
                    
                   }

               });
           });               
       });

   </script>

   <!-- Tỉnh/thành phố -->

<script src='https://cdn.jsdelivr.net/gh/vietblogdao/js/districts.min.js'></script>
<script>//<![CDATA[
// if (address_2 = localStorage.getItem('address_2_saved')) {
//   $('select[name="calc_shipping_district"] option').each(function() {
//     if ($(this).text() == address_2) {
//       $(this).attr('selected', '')
//     }
//   })
//   $('input.billing_address_2').attr('value', address_2)
// }
// if (district = localStorage.getItem('district')) {
//   $('select[name="calc_shipping_district"]').html(district)
//   $('select[name="calc_shipping_district"]').on('change', function() {
//     var target = $(this).children('option:selected')
//     target.attr('selected', '')
//     $('select[name="calc_shipping_district"] option').not(target).removeAttr('selected')
//     address_2 = target.text()
//     $('input.billing_address_2').attr('value', address_2)
//     district = $('select[name="calc_shipping_district"]').html()
//     localStorage.setItem('district', district)
//     localStorage.setItem('address_2_saved', address_2)
//   })
// }
$('select[name="calc_shipping_provinces"]').each(function() {
  var $this = $(this),
    stc = ''
  c.forEach(function(i, e) {
    e += +1
    stc += '<option value=' + e + '>' + i + '</option>'
    $this.html('<option value="">Tỉnh / Thành phố</option>' + stc)
    if (address_1 = localStorage.getItem('address_1_saved')) {
      $('select[name="calc_shipping_provinces"] option').each(function() {
        if ($(this).text() == address_1) {
          $(this).attr('selected', '')
        }
      })
      $('input.billing_address_1').attr('value', address_1)
    }
    // $this.on('change', function(i) {
    //   i = $this.children('option:selected').index() - 1
    //   var str = '',
    //     r = $this.val()
    //   if (r != '') {
    //     arr[i].forEach(function(el) {
    //       str += '<option value="' + el + '">' + el + '</option>'
    //       $('select[name="calc_shipping_district"]').html('<option value="">Quận / Huyện</option>' + str)
    //     })
    //     var address_1 = $this.children('option:selected').text()
    //     var district = $('select[name="calc_shipping_district"]').html()
    //     localStorage.setItem('address_1_saved', address_1)
    //     localStorage.setItem('district', district)
    //     $('select[name="calc_shipping_district"]').on('change', function() {
    //       var target = $(this).children('option:selected')
    //       target.attr('selected', '')
    //       $('select[name="calc_shipping_district"] option').not(target).removeAttr('selected')
    //       var address_2 = target.text()
    //       $('input.billing_address_2').attr('value', address_2)
    //       district = $('select[name="calc_shipping_district"]').html()
    //       localStorage.setItem('district', district)
    //       localStorage.setItem('address_2_saved', address_2)
    //     })
    //   } else {
    //     $('select[name="calc_shipping_district"]').html('<option value="">Quận / Huyện</option>')
    //     district = $('select[name="calc_shipping_district"]').html()
    //     localStorage.setItem('district', district)
    //     localStorage.removeItem('address_1_saved', address_1)
    //   }
   // })
  })
})
//]]></script>

  </body>
</html>
