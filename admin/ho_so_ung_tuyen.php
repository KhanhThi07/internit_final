<?php     
    include('include/header.php');
    include('include/sidebar.php');
    
?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="admin_dashboard.php">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="#">Hồ sơ ứng tuyển</a></li>
  </ol>
</nav>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 class="h2">Danh sách hồ sơ ứng tuyển</h1>
  
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
               
              </div>
             <!-- <a class="btn btn-primary" href="them_tintuyendung.php">Tạo tin tuyển dụng</a> -->
            </div>
          </div>

          <table id="example" class="display" style="width:100%">
        <thead>
            <tr>            
            <th>STT</th>

                <th>Tiêu đề</th>
                <!-- <th>Vị trí tuyển dụng</th> -->
                <th>Tên sinh viên</th>
                <th>Trình độ</th>
                <th>Trường học</th>
                <th>Số điện thoại</th>               
                <th>Ngày nộp</th>            
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
           
        <?php
            include('connection/db.php');        
            $a=1;
            $sql =mysqli_query($conn,"select * from ho_so_ung_tuyen 
                                    LEFT JOIN tin_tuyen_dung ON ho_so_ung_tuyen.ma_tuyendung= tin_tuyen_dung.ma_tuyendung  
                                    LEFT JOIN nhan_vien ON tin_tuyen_dung.ma_nhanvien= nhan_vien.ma_nhanvien   

                                    LEFT JOIN sinh_vien ON ho_so_ung_tuyen.ma_sinhvien= sinh_vien.ma_sinhvien                                                                                
                                    LEFT JOIN truong_hoc ON sinh_vien.ma_truonghoc= truong_hoc.ma_truonghoc   

                                    LEFT JOIN nguoi_dung ON nhan_vien.ma_taikhoan= nguoi_dung.ma_nd
                                    WHERE email='{$_SESSION['email']}'                                                                      

                           ");     
                             
                              $rowcount=mysqli_num_rows($sql);
                for($i=1;$i<=$rowcount;$i++)
             {
              $row=mysqli_fetch_array($sql);            
             ?>
            <tr>      
                <td><?php echo $a; ?></td>

                <td><?php echo $row['tieu_de'] ?></td>
                <td><?php echo $row['ho_ten'] ?></td>
                <td><?php echo $row['trinh_do'] ?></td>
                <td><?php echo $row['ten_truong'] ?></td>
                <td><?php echo $row['so_dien_thoai'] ?></td>
                <td><?php echo $row['ngay_nop'] ?></td>             
                <!-- <td><a href="http://localhost/final_intern_IT/files/<?php echo $row['file_cv']; ?>">Tải file</a></td>              -->

                <td>
                  <div class="row">
                    <div class="btn-group">
                      <a href="ho_so_ung_tuyen_xem.php?id=<?php echo $row['ma_hoso'];?>" class="btn btn-success"><span class="glyphicon glyphicon-eye-open"></span></a>
                      <!-- <a href="ho_so_ung_tuyen.php?del=<?php echo $row['ma_hoso'];?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a> -->

                    </div>

                  </div>
                </td>
             
                              
            </tr>
            <?php $a++;  } ?>    
         
              
        </tbody>
        <tfoot>
            <tr>       
            <th>STT</th>
                <th>Tiêu đề</th>
                <th>Tên sinh viên</th>
                <th>Trình độ</th>
                <th>Trường học</th>
                <th>Số điện thoại</th>               
                <th>Ngày nộp</th>            
                <th>Thao tác</th>
            </tr>
        </tfoot>
    </table>

          <canvas class="my-4" id="myChart" width="900" height="380"></canvas>

          
          <div class="table-responsive">
          
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    
<!-- datatables plugin -->
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   
   <script>
       $(document).ready(function() {
    $('#example').DataTable();
} );
   </script>
  </body>
</html>
