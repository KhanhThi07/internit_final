<?php
    include('connection/db.php');

    include('include/header.php');
    include('include/sidebar.php');

    $id = $_GET['edit'];

    $query = mysqli_query($conn,"SELECT * FROM nha_tuyen_dung WHERE ma_congty ='$id' ");

    while ($row = mysqli_fetch_array($query)) {      
        $ten_congty = $row['ten_congty'];
        $dia_chi = $row['dia_chi'];
        $quy_mo = $row['quy_mo'];
     
        $website =$row['website'];
        // $admin =$row['admin'];

    }

?>


<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="admin_dashboard.php">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="tao_nhatuyendung.php">Nhà tuyển dụng</a></li>
    <li class="breadcrumb-item"><a href="#">Cập nhật nhà tuyển dụng</a></li>
  </ol>
</nav>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1 class="h2">Cập nhật nhà tuyển dụng</h1>
  
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
               
              </div>
             <!-- <a class="btn btn-primary" href="them_nguoidung.php">Thêm người dùng</a> -->
            </div>
          </div>
          <div style="width: 60%; margin-left:20%; background-color: #F2F4F4;">
          <div id="msg"></div>
             <form action="" method="POST" style="margin: 3%; padding: 3%;" name="nha_tuyen_dung_form" id="nha_tuyen_dung_form">
                  <div class="form-group">
                      <label for="Ten nha tuyen dung">Tên công ty</label>
                      <input type="text" name="ten_congty" id="ten_congty" value="<?php echo  $ten_congty; ?>" class="form-control" placeholder="Nhập tên công ty">
                  </div>
                  <div class="form-group">
                      <label for="Dia chi nha tuyen dung">Địa chỉ</label>
                      <input type="text" name="dia_chi" id="dia_chi" value="<?php echo  $dia_chi; ?>" class="form-control" placeholder="Nhập địa chỉ">
                  </div>
                  <div class="form-group">
                      <label for="Quy mo nha tuyen dung">Quy mô</label>
                      <input type="text" name="quy_mo" id="quy_mo" value="<?php echo  $quy_mo; ?>" class="form-control" placeholder="Nhập quy mô">
                  </div>
            
                  <div class="form-group">
                      <label for="Dia chi website">Website</label>
                      <!-- <textarea name="website" id="website" value="<?php echo  $website; ?>" class="form-control" placeholder="Địa chỉ website" cols="30" rows="3"></textarea> -->
                      <input type="text" name="website" id="website" value="<?php echo  $website; ?>"  class="form-control" placeholder="Nhập địa chỉ website">
                  </div>
                  <!-- <div class="form-group">
                      <label for="">Chọn admin nhà tuyển dụng</label>
                      <select name="admin" class="form-control" id="admin">
                          <?php
                            include('connection/db.php');
                            $sql = mysqli_query($conn,"select * from nguoi_dung where vai_tro='2'");

                            while ($row=mysqli_fetch_array($sql)) { ?>
                                <option value="<?php echo $row['email'] ;?>"> <?php echo $row['email'] ;?> </option>
                          <?php  } ?>
                          ?>
                      </select>      
                     
                  </div>   -->

                  <input type="hidden" name="id" id="id" value=" <?php echo $_GET['edit']; ?> ">
                 

                  <div class="form-group" >
                         <input type="submit" class="btn btn-block btn-success" placeholder="Save" value="Lưu" name="submit" id="submit">

                     </div>
                  <!-- <div class="form-group">
                      <label for="Ảnh người dùng">Hình ảnh</label>
                      <input type="text" class="form-control" placeholder="Hình ảnh">
                  </div> -->

              </form>

          </div>

         

          <canvas class="my-4" id="myChart" width="900" height="380"></canvas>          
          <div class="table-responsive">
          
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    
<!-- datatables plugin -->
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
   
   <script>
       $(document).ready(function() {
    $('#example').DataTable();
} );
   </script>

  

  </body>
</html>

<?php
    include('connection/db.php');

    if (isset($_POST['submit'])) {
        $id = $_POST['id'];

        $ten_congty = $_POST['ten_congty'];
        $dia_chi = $_POST['dia_chi'];
        $quy_mo = $_POST['quy_mo'];
      
        $website = $_POST['website'];
        // $admin = $_POST['admin'];

        $query1 = mysqli_query($conn,"update nha_tuyen_dung set ten_congty= '$ten_congty',quy_mo= '$quy_mo',
        dia_chi= '$dia_chi', website= '$website'
         where ma_congty ='$id'");

        if ($query1) {
            echo "<script>alert('Cập nhật thành công!!!');</script>";
        }else{
        echo "<script>alert('Lỗi rồi, vui lòng thử lại!!!!');</script>";
        }
    }

?>